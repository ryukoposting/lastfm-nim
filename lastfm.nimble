# Package

version       = "0.6.0"
author        = "ryukoposting"
description   = "Last.FM API breakouts (documentation: http://ryuk.ooo/nimdocs/lastfm/lastfm.html)"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.2"

task test, "run all tests":
  withDir "tests":
    exec "nim c -d:ssl -r test1.nim"

task testjs, "try build with js":
  withDir "src":
    exec "nim js -d:js lastfm.nim"

task docs, "generate docs!":
  withDir "src":
    exec "nim doc -d:ssl lastfm.nim || true"
    exec "rm -rf ../docs/*.html"
    exec "mv *.html ../docs"
    
    exec "nim doc -d:ssl lastfm/track.nim || true"
    exec "nim doc -d:ssl lastfm/artist.nim || true"
    exec "nim doc -d:ssl lastfm/album.nim || true"
    exec "nim doc -d:ssl lastfm/tag.nim || true"
    exec "nim doc -d:ssl lastfm/chart.nim || true"
    exec "nim doc -d:ssl lastfm/geo.nim || true"
    exec "nim doc -d:ssl lastfm/user.nim || true"
    exec "rm -rf ../docs/lastfm/*.html"
    exec "mv *.html ../docs/lastfm"
