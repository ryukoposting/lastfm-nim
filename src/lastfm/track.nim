## `back<../lastfm.html>`_

when defined(js):
  import jsffi, asyncjs, times

when not defined(js):
  import httpclient, asyncdispatch
  import md5, json, uri, sequtils, strformat, os, times

import private/tools


proc trackCorrection*(
  fm: LastFM,
  track, artist: string): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getCorrection<https://www.last.fm/api/show/track.getCorrection>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getCorrection",
                    ("track", track),
                    ("artist", artist))
  result = parseJson(await fm.http.postContent(url))


proc trackInfo*(
  fm: LastFM,
  track, artist: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getInfo<https://www.last.fm/api/show/track.getInfo>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getInfo",
                    ("track", track),
                    ("artist", artist),
                    ("autocorrect", if autocorrect: "1" else: "0"))
  result = parseJson(await fm.http.postContent(url))


proc trackInfo*(
  fm: LastFM,
  mbid: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getInfo<https://www.last.fm/api/show/track.getInfo>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getInfo",
                    ("mbid", mbid),
                    ("autocorrect", if autocorrect: "1" else: "0"))
  result = parseJson(await fm.http.postContent(url))


proc searchTracks*(
  fm: LastFM,
  track, artist: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getInfo<https://www.last.fm/api/show/track.search>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.search",
                    ("track", track),
                    ("artist", artist),
                    ("autocorrect", if autocorrect: "1" else: "0"))
  result = parseJson(await fm.http.postContent(url))  # ["results"]["trackmatches"]["track"]


proc searchTracks*(
  fm: LastFM,
  track: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getInfo<https://www.last.fm/api/show/track.search>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.search",
                    ("track", track),
                    ("autocorrect", if autocorrect: "1" else: "0"))
  result = parseJson(await fm.http.postContent(url))  # ["results"]["trackmatches"]["track"]


# TODO: implement album, context, streamid, chosenByUser, mbid, duration, duration
proc scrobble*(
  fm: LastFMSession,
  track, artist: string, 
  timestamp = getTime().toUnix()): Future[JsonNode] {.async.} =
  ## Breakout of `Track.scrobble<https://www.last.fm/api/show/track.scrobble>`_.
  ## Currently, multiple fields are missing from this API, including album
  ## name, sub-client name, streamid, chosenByUser, duration, and MusicBrainz ID.
  ## However, the basic functionality (scrobbling a track from its name and
  ## artist) is fully functional.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.scrobble",
                        ("track", track),
                        ("artist", artist),
                        ("timestamp", $timestamp))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc loveTrack*(
  fm: LastFMSession,
  track, artist: string): Future[JsonNode] {.async.} =
  ## Breakout of `Track.love<https://www.last.fm/api/show/track.love>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.love",
                        ("track", track),
                        ("artist", artist))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc unloveTrack*(
  fm: LastFMSession,
  track, artist: string): Future[JsonNode] {.async.} =
  ## Breakout of `Track.unlove<https://www.last.fm/api/show/track.unlove>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.unlove",
                        ("track", track),
                        ("artist", artist))
    content = await fm.http.postContent(url)
  result = parseJson(content)


# TODO: implement missing optional args
proc setNowPlaying*(
  fm: LastFMSession,
  track, artist: string): Future[JsonNode] {.async.} =
  ## Breakout of `Track.updateNowPlaying<https://www.last.fm/api/show/track.updateNowPlaying>`_.
  ## Currently missing optional fields. However, core functionality is working.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.updateNowPlaying",
                        ("track", track),
                        ("artist", artist))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc similarTracks*(
  fm: LastFM,
  track, artist: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.unlove<https://www.last.fm/api/show/track.updateNowPlaying>`_.
  ## Currently missing optional fields. However, core functionality is working.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getSimilar",
                    ("track", track),
                    ("artist", artist),
                    ("autocorrect", if autocorrect: "1" else: "0"))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc trackAddTags*(
  fm: LastFMSession,
  track, artist: string,
  tags: AtMost[10, string]): Future[JsonNode] {.async.} =
  ## Breakout of `Track.addTags<https://www.last.fm/api/show/track.addTags>`_.
  ## `tags` is of type AtMost[10, string], which is any array of strings with
  ## length <= 10.
  ## Returns the JsonNode of the contents of the response.
  let
    tagstr = foldl(tags, a & "," & b)
    url = fm.genAuthUrl("track.addTags",
                        ("track", track),
                        ("artist", artist),
                        ("tags", tagstr))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc trackRemoveTag*(
  fm: LastFMSession,
  track, artist: string,
  tag: string): Future[JsonNode] {.async.} =
  ## Breakout of `Track.removeTags<https://www.last.fm/api/show/track.removeTag>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.removeTag",
                        ("track", track),
                        ("artist", artist),
                        ("tag", tag))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc trackTags*(
  fm: LastFM,
  track, artist, user: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getTags<https://www.last.fm/api/show/track.getTags>`_.
  ## This version of the function may be used for both unauthenticated and authenticated
  ## sessions, and thus requires that a user parameter be specified.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getTags",
                    ("track", track),
                    ("artist", artist),
                    ("user", user),
                    ("autocorrect", if autocorrect: "1" else: "0"))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc trackTags*(
  fm: LastFMSession,
  track, artist: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getTags<https://www.last.fm/api/show/track.getTags>`_.
  ## This version of the function can only be used with an authenticated Last.FM session.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genAuthUrl("track.getTags",
                        ("track", track),
                        ("artist", artist),
                        ("autocorrect", if autocorrect: "1" else: "0"))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc trackTopTags*(
  fm: LastFM,
  track, artist: string,
  autocorrect = on): Future[JsonNode] {.async.} =
  ## Breakout of `Track.getTopTags<https://www.last.fm/api/show/track.getTopTags>`_.
  ## Currently missing the optional mbid field.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("track.getTopTags",
                    ("track", track),
                    ("artist", artist),
                    ("autocorrect", if autocorrect: "1" else: "0"))
    content = await fm.http.postContent(url)
  result = parseJson(content)
