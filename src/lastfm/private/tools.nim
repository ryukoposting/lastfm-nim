when not defined(nimDoc):
  when not defined(js):
    import httpclient, asyncfutures, uri, json, algorithm, sequtils, md5, macros, sugar
  
  when defined(js):
    import jshttp, jsmd5, sequtils, algorithm, uri, sugar
  
  type
    AtMost*[L: static int; T] = concept TS
      TS[low(TS)] is T
      TS.len() <= L
    
    LastFM* = ref object of RootObj
      key*, secret*: string
      http*: AsyncHttpClient
    
    LastFMSession* = ref object of LastFM
      sk*: string

  const
    lastFmUrl = "https://ws.audioscrobbler.com/2.0/"

  proc genSig(fm: LastFMSession, methd: string, params: var seq[(string, string)]): string =
    var hashstring = ""
    params.add ("method", methd)
    params.add ("api_key", fm.key)
    params.add ("sk", fm.sk)
    params.sort((x, y) => cmp(x[0], y[0]))
    for p in params:
      hashstring.add p[0]
      hashstring.add p[1]
    hashstring.add fm.secret
    result = $toMD5(hashstring)

  proc genUrl*(fm: LastFM, methd: string, strs: varargs[(string,string)]): string =
    result = "?method=" & methd & "&api_key=" & fm.key & "&format=json&"
    for v in strs:
      result.add v[0] & "=" & encodeUrl(v[1]) & "&"
    result = lastFmUrl & result[0..result.high - 1]

  proc genAuthUrl*(fm: LastFMSession, methd: string, strs: varargs[(string, string)]): string =
    var ps = strs.map((x) => x)
    result = fm.genUrl(methd, strs) & "&sk=" & fm.sk & "&api_sig=" & fm.genSig(methd, ps)


# macro arrln(l, h: BiggestInt, typ: untyped): untyped =
#   if intVal(l) == intVal(h):
#     return nnkBracketExpr.newTree(
#       newIdentNode("array"), l, typ)
#   else:
#     let li: BiggestInt = intVal(l) + 1
#     return nnkInfix.newTree(
#       newIdentNode("|"),
#       arrln(li, intVal(h), typ), typ)
