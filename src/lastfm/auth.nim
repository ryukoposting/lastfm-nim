when defined(js):
  import private/jshttp, private/jsmd5
  import jsffi, asyncjs

when not defined(js):
  import httpclient, asyncdispatch
  import md5, json, uri, sequtils, strformat

import private/tools

proc LastFM*(apiKey, apiSecret: string): LastFM =
  ## Produces an un-authenticated Last.FM session that can be used to...
  ##
  ## - query any track, artist, or album metadata
  ## - query any public metadata about a Last.FM user account
  ## - generate one or more authenticated Last.FM sessions, which can
  ##   then be used for scrobbling, "favoriting," etc.
  ##
  result = LastFM(key: apiKey,
                  secret: apiSecret,
                  http: newAsyncHttpClient())


proc loadAuth*(fm: LastFM, sessionKey: string): LastFMSession =
  ## Generate a client-authenticated session from a pre-existing
  ## session key.
  ##
  ## Last.FM session keys do not expire, unless revoked by the user.
  ## As such, an application may get authorization using desktopAuth
  ## or mobileAuth, then save that session key in a file. The next
  ## time the user opens the application, this session key can be
  ## reused.
  result = LastFMSession(key: fm.key,
                         secret: fm.secret,
                         http: fm.http,
                         sk: sessionKey)


proc mobileAuth*(
  fm: LastFM,
  username, password: string): Future[LastFMSession] {.async.} =
  ## Generate a client-authenticated session following Last.FM's mobile
  ## authentication flow. Using this API is not recommended under normal
  ## circumstances, as it requires access to the user's password in
  ## plaintext.
  let
    reqstr = fm.genUrl("auth.getMobileSession",
                       ("username", username),
                       ("password", password))
    sig = $toMD5("api_key" & fm.key &
                 "methodauth.getMobileSessionpassword" &
                 password & "username" & username & fm.secret)
    req = await fm.http.postContent(reqstr & sig)
  result = LastFMSession(key: fm.key,
                         secret: fm.secret,
                         http: fm.http,
                         sk: parseJson(req)["session"]["key"].getStr)


proc desktopAuth*(
  fm: LastFM,
  keepGoing: proc(_: string): Future[bool]): Future[LastFMSession] {.async.} =
  ## Generate a client-authenticated session following Last.FM's "desktop"
  ## authentication flow.
  ##
  ## `keepGoing` is passed a URL that should be opened in the client's
  ## browser so that they can grant permission to your application.
  ## 
  ## - If the user successfully grants permission to your application,
  ##   keepGoing should return true.
  ## - If the user does NOT authorize your application, keepGoing must
  ##   return false. If keepGoing returns false, an IOError is thrown.
  ##
  let
    tokenreq = await fm.http.postContent(fm.genUrl("auth.gettoken"))
    token = tokenreq.parseJson()["token"].getStr
    didAuth = await keepGoing("http://www.last.fm/api/auth/?api_key=" &
                              fm.key & "&token=" & token)
  
  if not didAuth:
    raise newException(IOError, "future keepGoing gave false")
  else:
    let
      sessrequrl = fm.genUrl("auth.getSession", ("token", token))
      sig = $toMD5("api_key" & fm.key & "methodauth.getSessiontoken" &
                   token & fm.secret)
      req = await fm.http.postContent(sessrequrl & "&api_sig=" & sig)
    result = LastFMSession(key: fm.key,
                           secret: fm.secret,
                           http: fm.http,
                           sk: parseJson(req)["session"]["key"].getStr)
