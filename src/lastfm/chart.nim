## `back<../lastfm.html>`_

when defined(js):
  import jsffi, asyncjs

when not defined(js):
  import httpclient, asyncdispatch
  import md5, json, uri, sequtils, strformat

import private/tools


proc chartTopArtists*(
  fm: LastFM,
  page = 1,
  limit = 50): Future[JsonNode] {.async.} =
  ## Breakout of `chart.getTopArtists<https://www.last.fm/api/show/chart.getTopArtists>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("chart.getTopArtists",
                    ("limit", $limit),
                    ("page", $page))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc chartTopTags*(
  fm: LastFM,
  page = 1,
  limit = 50): Future[JsonNode] {.async.} =
  ## Breakout of `chart.getTopTags<https://www.last.fm/api/show/chart.getTopTags>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("chart.getTopTags",
                    ("limit", $limit),
                    ("page", $page))
    content = await fm.http.postContent(url)
  result = parseJson(content)


proc chartTopTracks*(
  fm: LastFM,
  page = 1,
  limit = 50): Future[JsonNode] {.async.} =
  ## Breakout of `chart.getTopTracks<https://www.last.fm/api/show/chart.getTopTracks>`_.
  ## Returns the JsonNode of the contents of the response.
  let
    url = fm.genUrl("chart.getTopTracks",
                    ("limit", $limit),
                    ("page", $page))
    content = await fm.http.postContent(url)
  result = parseJson(content)
