## .. code-block:: nim
##  import lastfm
##  import lastfm/[track, artist, album, tag, chart]
##
## lastfm is a Nim library for the Last.FM web API. It leverages async/await,
## and provides multiple methods for setting up client-authenticated sessions.
##
## lastfm consists of multiple submodules:
##
## - `artist<./lastfm/artist.html>`_
## - `track<./lastfm/track.html>`_
## - `album<./lastfm/album.html>`_
## - `tag<./lastfm/tag.html>`_
## - `chart<./lastfm/chart.html>`_
## - `geo<./lastfm/geo.html>`_
## - `user<./lastfm/user.html>`_
##

# TODO: add support for JS toolchain?
# TODO: add support for JS toolchain?
# TODO: add support for JS toolchain?

when defined(js):
  import jsffi, asyncjs

when not defined(js):
  import httpclient, asyncdispatch
  import md5, json, uri, sequtils, strformat

include lastfm/auth
import lastfm/[track, artist, album, tag, chart, geo, user]

export LastFM, LastFMSession
